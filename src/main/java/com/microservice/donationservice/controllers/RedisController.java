package com.microservice.donationservice.controllers;

import com.microservice.donationservice.services.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class RedisController {
    private final RedisService redisService;

    @Autowired
    public RedisController(RedisService redisService) {
        this.redisService = redisService;
    }

    @PostMapping("/data")
    public void setData(@RequestParam String key, @RequestParam String value) {
        redisService.set(key, value);
    }

    @GetMapping("/data/{key}")
    public String getData(@PathVariable String key) {
        return redisService.get(key);
    }
}
